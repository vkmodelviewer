//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
// Developed by Minigraph
//
// Author:  James Stanard
//

#include "pch.h"
#include "SystemTime.h"
#include <time.h>

double SystemTime::sm_CpuTickDelta = 0.0;

// Query the performance counter frequency
void SystemTime::Initialize( void )
{
    sm_CpuTickDelta = 1.0 / 10000000.0;
}

// Query the current value of the performance counter
int64_t SystemTime::GetCurrentTick( void )
{
    struct timespec ts;

    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    return ts.tv_sec * (int64_t)10000000 + ts.tv_nsec / 100;
}

void SystemTime::BusyLoopSleep( float SleepTime )
{
    int64_t finalTick = GetCurrentTick() + (SleepTime * 10000000.0);
    while (GetCurrentTick() < finalTick);
}

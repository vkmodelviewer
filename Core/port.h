/*
 * Copyright 2017 Henri Verbeet for CodeWeavers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef PORT_H
#define PORT_H

#include <errno.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <xmmintrin.h>
#include <xcb/xcb_event.h>
#include <vkd3d_windows.h>
#include <vkd3d_utils.h>
#include <vkd3d_dxgiformat.h>
#include "xinput/xinput.h"

#define INFINITE VKD3D_INFINITE

#define PORT_KEY_space          0x0020
#define PORT_KEY_apostrophe     0x0027
#define PORT_KEY_comma          0x002c
#define PORT_KEY_minus          0x002d
#define PORT_KEY_period         0x002e
#define PORT_KEY_slash          0x002f
#define PORT_KEY_0              0x0030
#define PORT_KEY_1              0x0031
#define PORT_KEY_2              0x0032
#define PORT_KEY_3              0x0033
#define PORT_KEY_4              0x0034
#define PORT_KEY_5              0x0035
#define PORT_KEY_6              0x0036
#define PORT_KEY_7              0x0037
#define PORT_KEY_8              0x0038
#define PORT_KEY_9              0x0039
#define PORT_KEY_semicolon      0x003b
#define PORT_KEY_equal          0x003d
#define PORT_KEY_bracketleft    0x005b
#define PORT_KEY_backslash      0x005c
#define PORT_KEY_bracketright   0x005d
#define PORT_KEY_grave          0x0060
#define PORT_KEY_a              0x0061
#define PORT_KEY_b              0x0062
#define PORT_KEY_c              0x0063
#define PORT_KEY_d              0x0064
#define PORT_KEY_e              0x0065
#define PORT_KEY_f              0x0066
#define PORT_KEY_g              0x0067
#define PORT_KEY_h              0x0068
#define PORT_KEY_i              0x0069
#define PORT_KEY_j              0x006a
#define PORT_KEY_k              0x006b
#define PORT_KEY_l              0x006c
#define PORT_KEY_m              0x006d
#define PORT_KEY_n              0x006e
#define PORT_KEY_o              0x006f
#define PORT_KEY_p              0x0070
#define PORT_KEY_q              0x0071
#define PORT_KEY_r              0x0072
#define PORT_KEY_s              0x0073
#define PORT_KEY_t              0x0074
#define PORT_KEY_u              0x0075
#define PORT_KEY_v              0x0076
#define PORT_KEY_w              0x0077
#define PORT_KEY_x              0x0078
#define PORT_KEY_y              0x0079
#define PORT_KEY_z              0x007a
#define PORT_KEY_BackSpace      0xff08
#define PORT_KEY_Tab            0xff09
#define PORT_KEY_Return         0xff0d
#define PORT_KEY_Pause          0xff13
#define PORT_KEY_Scroll_Lock    0xff14
#define PORT_KEY_Escape         0xff1b
#define PORT_KEY_Home           0xff50
#define PORT_KEY_Left           0xff51
#define PORT_KEY_Up             0xff52
#define PORT_KEY_Right          0xff53
#define PORT_KEY_Down           0xff54
#define PORT_KEY_Page_Up        0xff55
#define PORT_KEY_Page_Down      0xff56
#define PORT_KEY_End            0xff57
#define PORT_KEY_Print          0xff61
#define PORT_KEY_Insert         0xff63
#define PORT_KEY_Menu           0xff67
#define PORT_KEY_Num_Lock       0xff7f
#define PORT_KEY_KP_Enter       0xff8d
#define PORT_KEY_KP_Home        0xff95
#define PORT_KEY_KP_Left        0xff96
#define PORT_KEY_KP_Up          0xff97
#define PORT_KEY_KP_Right       0xff98
#define PORT_KEY_KP_Down        0xff99
#define PORT_KEY_KP_Page_Up     0xff9a
#define PORT_KEY_KP_Page_Down   0xff9b
#define PORT_KEY_KP_End         0xff9c
#define PORT_KEY_KP_Begin       0xff9d
#define PORT_KEY_KP_Insert      0xff9e
#define PORT_KEY_KP_Delete      0xff9f
#define PORT_KEY_KP_Multiply    0xffaa
#define PORT_KEY_KP_Add         0xffab
#define PORT_KEY_KP_Subtract    0xffad
#define PORT_KEY_KP_Divide      0xffaf
#define PORT_KEY_F1             0xffbe
#define PORT_KEY_F2             0xffbf
#define PORT_KEY_F3             0xffc0
#define PORT_KEY_F4             0xffc1
#define PORT_KEY_F5             0xffc2
#define PORT_KEY_F6             0xffc3
#define PORT_KEY_F7             0xffc4
#define PORT_KEY_F8             0xffc5
#define PORT_KEY_F9             0xffc6
#define PORT_KEY_F10            0xffc7
#define PORT_KEY_F11            0xffc8
#define PORT_KEY_F12            0xffc9
#define PORT_KEY_Shift_L        0xffe1
#define PORT_KEY_Shift_R        0xffe2
#define PORT_KEY_Control_L      0xffe3
#define PORT_KEY_Control_R      0xffe4
#define PORT_KEY_Caps_Lock      0xffe5
#define PORT_KEY_Alt_L          0xffe9
#define PORT_KEY_Alt_R          0xffea
#define PORT_KEY_Super_L        0xffeb
#define PORT_KEY_Super_R        0xffec
#define PORT_KEY_Delete         0xffff

/* SAL */
#define _Always_(x)
#define _Analysis_assume_
#define _COM_Outptr_
#define _In_
#define _In_opt_
#define _In_range_(x, y)
#define _In_reads_(x)
#define _In_reads_bytes_(x)
#define _In_reads_opt_(x)
#define _In_z_
#define _Inout_
#define _Out_
#define _Out_opt_
#define _Out_writes_(x)
#define _Out_writes_bytes_(x)
#define _Outptr_
#define _Outptr_opt_
#define _Success_(x)
#define _Use_decl_annotations_

#define __forceinline inline __attribute__((always_inline))
#define _countof(x) (sizeof(x) / sizeof((x)[0]))

#if defined(__x86_64__)
# define __cdecl __attribute__((ms_abi))
# define __fastcall __attribute__((ms_abi))
#elif defined(__i386__)
# define __cdecl __attribute__((cdecl))
# define __fastcall __attribute__((fastcall))
#endif

#define PORT_PRINTF(fmt, args) __attribute__((format(printf, fmt, args)))

#define WINAPI_PARTITION_DESKTOP (1)
#define WINAPI_FAMILY_PARTITION(x) (x)

#define IID_PPV_ARGS(x) __uuidof(**x), (void **)x

#define HEAP_ZERO_MEMORY 0x8

#define INVALID_HANDLE_VALUE (void *)~0u

#define FACILITY_WIN32 0x7

#define ERROR_SUCCESS       0x00
#define ERROR_INVALID_DATA  0x0d
#define ERROR_HANDLE_EOF    0x26
#define ERROR_NOT_SUPPORTED 0x32

typedef unsigned char byte;

struct port_mouse_state
{
    double x, y, z;
    uint32_t buttons;
};

struct port
{
    xcb_connection_t *connection;
    xcb_atom_t wm_protocols_atom;
    xcb_atom_t wm_delete_window_atom;
    int screen;

    struct port_window **windows;
    size_t windows_size;
    size_t window_count;

    void *user_data;
    void (*idle_func)(struct port *port, void *user_data);

    uint8_t xi_opcode;
    xcb_input_device_id_t xi_client_pointer;
    xcb_input_device_id_t xi_current_slave;
    struct
    {
        unsigned int x, y;
    } mouse_axes;
    struct port_mouse_state mouse;
    char keys[32];
};

struct port_swapchain_desc
{
    unsigned int width;
    unsigned int height;
    unsigned int buffer_count;
    DXGI_FORMAT format;
};

struct port_key_map
{
    size_t base;
    size_t count;
    uint32_t symbols[];
};

static inline HRESULT HRESULT_FROM_WIN32(unsigned int x)
{
    if ((int)x <= 0)
        return x;
    return 0x80000000 | (FACILITY_WIN32 << 16) | (x & 0x0000ffff);
}

static inline void __debugbreak(void)
{
    __asm__ __volatile__("int3");
}

static inline unsigned char _BitScanForward64(unsigned long *index, uint64_t mask)
{
    if (!mask)
    {
        *index = 0;
        return 0;
    }
    *index = __builtin_ctzll(mask);
    return 1;
}

static inline unsigned char _BitScanForward(unsigned long *index, unsigned long mask)
{
    if (!mask)
    {
        *index = 0;
        return 0;
    }
    *index = __builtin_ctzl(mask);
    return 1;
}

static inline unsigned char _BitScanReverse(unsigned long *index, unsigned long mask)
{
    if (!mask)
    {
        *index = 0;
        return 0;
    }
    *index = __builtin_clzl(mask) ^ ((8 * sizeof(mask)) - 1);
    return 1;
}

static inline void ZeroMemory(void *p, SIZE_T s)
{
    memset(p, 0, s);
}

static inline HANDLE GetProcessHeap(void)
{
    return nullptr;
}

static inline void *HeapAlloc(HANDLE heap, DWORD flags, SIZE_T s)
{
    void *p;

    p = malloc(s);
    if (flags & HEAP_ZERO_MEMORY)
        memset(p, 0, s);
    return p;
}

static inline void HeapFree(HANDLE heap, DWORD flags, void *p)
{
    free(p);
}

static inline int vsprintf_s(char *str, size_t size, const char *format, va_list args)
{
    int ret;

    ret = vsnprintf(str, size, format, args);
    if (ret < 0 || (size_t)ret >= size)
        return -1;
    return ret;
}

static inline int _stricmp(const char *p, const char *q)
{
    return strcasecmp(p, q);
}

template <size_t size> int strcpy_s(char (&dst)[size], const char *src)
{
    if (strlen(src) >= size)
        return ERANGE;
    strcpy(dst, src);
    return 0;
}

static inline int fopen_s(FILE **f, const char *path, const char *mode)
{
    if (!(*f = fopen(path, mode)))
        return errno;
    return 0;
}

static inline HANDLE port_create_event(void)
{
    return vkd3d_create_event();
}

static inline unsigned int port_wait_event(HANDLE event, unsigned int ms)
{
    return vkd3d_wait_event(event, ms);
}

static inline void port_destroy_event(HANDLE event)
{
    vkd3d_destroy_event(event);
}

void port_cleanup(struct port *port);
struct port_key_map *port_get_key_map(struct port *port);
bool port_get_key_state(const struct port *port, unsigned int key);
void port_get_mouse_state(struct port *port, struct port_mouse_state *s);
void port_grab_pointer(struct port *port, struct port_window *window);
bool port_init(struct port *port, void *user_data);
void port_process_events(struct port *port);
void port_set_idle_func(struct port *port, void (*idle_func)(struct port *port, void *user_data));
struct port_swapchain *port_swapchain_create(struct ID3D12CommandQueue *command_queue,
        struct port_window *window, const struct port_swapchain_desc *desc);
void port_swapchain_destroy(struct port_swapchain *swapchain);
struct ID3D12Resource *port_swapchain_get_back_buffer(struct port_swapchain *swapchain, unsigned int index);
void port_swapchain_present(struct port_swapchain *swapchain);
void port_ungrab_pointer(struct port *port, struct port_window *window);
struct port_window *port_window_create(struct port *port, const char *title,
        unsigned int width, unsigned int height, void *user_data);
void port_window_destroy(struct port_window *window);

#endif /* PORT_H */

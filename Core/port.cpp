/*
 * Copyright 2016 Józef Kucia for CodeWeavers
 * Copyright 2016, 2017 Henri Verbeet for CodeWeavers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#define VK_USE_PLATFORM_XCB_KHR
#include "port.h"
#include <vkd3d.h>
#include <xcb/xcb_icccm.h>
#include "xinput/xinput.h"

struct port_window
{
    xcb_window_t window;
    struct port *port;
};

struct port_swapchain
{
    VkSurfaceKHR vk_surface;
    VkSwapchainKHR vk_swapchain;
    VkFence vk_fence;

    VkInstance vk_instance;
    VkDevice vk_device;
    ID3D12CommandQueue *command_queue;

    uint32_t current_buffer;
    unsigned int buffer_count;
    ID3D12Resource *buffers[1];
};

static xcb_atom_t port_get_atom(xcb_connection_t *c, const char *name)
{
    xcb_intern_atom_cookie_t cookie;
    xcb_intern_atom_reply_t *reply;
    xcb_atom_t atom = XCB_NONE;

    cookie = xcb_intern_atom(c, 0, strlen(name), name);
    if ((reply = xcb_intern_atom_reply(c, cookie, NULL)))
    {
        atom = reply->atom;
        free(reply);
    }

    return atom;
}

static bool port_add_window(struct port *port, struct port_window *window)
{
    if (port->window_count == port->windows_size)
    {
        size_t new_capacity;
        void *new_elements;

        new_capacity = max(port->windows_size * 2, (size_t)4);
        if (!(new_elements = realloc(port->windows, new_capacity * sizeof(*port->windows))))
            return false;
        port->windows = (struct port_window **)new_elements;
        port->windows_size = new_capacity;
    }

    port->windows[port->window_count++] = window;

    return true;
}

static void port_remove_window(struct port *port, const struct port_window *window)
{
    size_t i;

    for (i = 0; i < port->window_count; ++i)
    {
        if (port->windows[i] != window)
            continue;

        --port->window_count;
        memmove(&port->windows[i], &port->windows[i + 1], (port->window_count - i) * sizeof(*port->windows));
        break;
    }
}

static struct port_window *port_find_window(struct port *port, xcb_window_t window)
{
    size_t i;

    for (i = 0; i < port->window_count; ++i)
    {
        if (port->windows[i]->window == window)
            return port->windows[i];
    }

    return NULL;
}

static xcb_screen_t *port_get_screen(xcb_connection_t *c, int idx)
{
    xcb_screen_iterator_t iter;

    iter = xcb_setup_roots_iterator(xcb_get_setup(c));
    for (; iter.rem; xcb_screen_next(&iter), --idx)
    {
        if (!idx)
            return iter.data;
    }

    return NULL;
}

struct port_window *port_window_create(struct port *port, const char *title,
        unsigned int width, unsigned int height, void *user_data)
{
    static const uint32_t window_events = XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_KEY_RELEASE;

    struct port_window *window;
    xcb_size_hints_t hints;
    xcb_screen_t *screen;

    if (!(screen = port_get_screen(port->connection, port->screen)))
        return NULL;

    if (!(window = (struct port_window *)malloc(sizeof(*window))))
        return NULL;

    if (!port_add_window(port, window))
    {
        free(window);
        return NULL;
    }

    window->window = xcb_generate_id(port->connection);
    window->port = port;
    xcb_create_window(port->connection, XCB_COPY_FROM_PARENT, window->window, screen->root, 0, 0,
            width, height, 0, XCB_WINDOW_CLASS_INPUT_OUTPUT, screen->root_visual,
            XCB_CW_EVENT_MASK, &window_events);
    xcb_change_property(port->connection, XCB_PROP_MODE_REPLACE, window->window, XCB_ATOM_WM_NAME,
            XCB_ATOM_STRING, 8, strlen(title), title);
    xcb_change_property(port->connection, XCB_PROP_MODE_REPLACE, window->window, port->wm_protocols_atom,
            XCB_ATOM_ATOM, 32, 1, &port->wm_delete_window_atom);
    hints.flags = XCB_ICCCM_SIZE_HINT_P_MIN_SIZE | XCB_ICCCM_SIZE_HINT_P_MAX_SIZE;
    hints.min_width = width;
    hints.min_height = height;
    hints.max_width = width;
    hints.max_height = height;
    xcb_change_property(port->connection, XCB_PROP_MODE_REPLACE, window->window, XCB_ATOM_WM_NORMAL_HINTS,
            XCB_ATOM_WM_SIZE_HINTS, 32, sizeof(hints) >> 2, &hints);

    xcb_map_window(port->connection, window->window);

    xcb_flush(port->connection);

    return window;
}

void port_window_destroy(struct port_window *window)
{
    xcb_destroy_window(window->port->connection, window->window);
    xcb_flush(window->port->connection);
    port_remove_window(window->port, window);
    free(window);
}

static void port_update_mouse_axes(struct port *port, xcb_input_device_class_iterator_t *it)
{
    unsigned int idx = 0;

    port->mouse_axes.x = ~0u;
    port->mouse_axes.y = ~0u;

    for (; it->rem; xcb_input_device_class_next(it))
    {
        xcb_input_valuator_class_t *v = (xcb_input_valuator_class_t *)it->data;

        if (v->type != XCB_INPUT_INPUT_CLASS_VALUATOR || v->mode != XCB_INPUT_VALUATOR_MODE_RELATIVE)
            continue;

        switch (idx++)
        {
            case 0:
                port->mouse_axes.x = v->number;
                break;

            case 1:
                port->mouse_axes.y = v->number;
                break;

            default:
                return;
        }
    }
}

static void port_process_xi_raw_motion_event(struct port *port, const xcb_input_raw_motion_event_t *motion_event)
{
    const xcb_input_fp3232_t *values;
    unsigned int i, j, k, idx;
    uint32_t *mask;
    double v;

    if (!port->xi_client_pointer)
    {
        xcb_input_xi_get_client_pointer_cookie_t gcp_cookie;
        xcb_input_xi_get_client_pointer_reply_t *gcp_reply;
        xcb_input_xi_query_device_cookie_t query_cookie;
        xcb_input_xi_query_device_reply_t *query_reply;
        xcb_input_xi_device_info_iterator_t it;
        xcb_input_device_id_t client_pointer;

        gcp_cookie = xcb_input_xi_get_client_pointer(port->connection, XCB_WINDOW_NONE);
        if (!(gcp_reply = xcb_input_xi_get_client_pointer_reply(port->connection, gcp_cookie, NULL))
                || !gcp_reply->set)
        {
            free(gcp_reply);
            return;
        }

        client_pointer = gcp_reply->deviceid;
        free(gcp_reply);

        query_cookie = xcb_input_xi_query_device(port->connection, XCB_INPUT_DEVICE_ALL);
        if (!(query_reply = xcb_input_xi_query_device_reply(port->connection, query_cookie, NULL)))
            return;

        it = xcb_input_xi_query_device_infos_iterator(query_reply);
        for (; it.rem; xcb_input_xi_device_info_next(&it))
        {
            xcb_input_device_class_iterator_t class_it;
            xcb_input_xi_device_info_t *info = it.data;

            if (info->deviceid != motion_event->deviceid)
                continue;
            if (info->type != XCB_INPUT_DEVICE_TYPE_SLAVE_POINTER)
                continue;
            if (info->attachment != client_pointer)
                continue;
            port->xi_current_slave = info->deviceid;

            class_it = xcb_input_xi_device_info_classes_iterator(info);
            port_update_mouse_axes(port, &class_it);

            break;
        }
        free(query_reply);

        if (!port->xi_current_slave)
            return;
        port->xi_client_pointer = client_pointer;
    }

    if (motion_event->deviceid != port->xi_current_slave)
        return;

    mask = (uint32_t *)(motion_event + 1);
    values = (const xcb_input_fp3232_t *)(&mask[motion_event->valuators_len]);
    for (i = 0, idx = 0; i < motion_event->valuators_len; ++i)
    {
        uint32_t m = mask[i];

        while (m)
        {
            j = __builtin_ctz(m);
            m &= ~(1u << j);
            k = (i << 5) + j;
            v = values[idx].integral + (double)values[idx].frac / ~0u;

            if (k == port->mouse_axes.x)
                port->mouse.x += v;
            else if (k == port->mouse_axes.y)
                port->mouse.y += v;
            ++idx;
        }
    }
}

static void port_process_xi_event(struct port *port, const xcb_ge_generic_event_t *event)
{
    const xcb_input_device_changed_event_t *change_event;
    xcb_input_device_class_iterator_t class_it;

    switch (event->event_type)
    {
        case XCB_INPUT_DEVICE_CHANGED:
            change_event = (const xcb_input_device_changed_event_t *)event;
            if (change_event->deviceid != port->xi_client_pointer
                    || change_event->reason != XCB_INPUT_CHANGE_REASON_SLAVE_SWITCH)
                break;
            class_it.data = (xcb_input_device_class_t *)&change_event[1];
            class_it.rem = change_event->num_classes;
            class_it.index = (char *)class_it.data - (char *)change_event;
            port_update_mouse_axes(port, &class_it);
            port->xi_current_slave = change_event->sourceid;
            break;

        case XCB_INPUT_RAW_MOTION:
            port_process_xi_raw_motion_event(port, (const xcb_input_raw_motion_event_t *)event);
            break;

        default:
            fprintf(stderr, "Unhandled X Input event type %#x.\n", event->event_type);
            break;
    }
}

void port_process_events(struct port *port)
{
    const struct xcb_client_message_event_t *client_message;
    const xcb_ge_generic_event_t *generic_event;
    const xcb_key_release_event_t *key_release;
    const xcb_key_press_event_t *key_press;
    xcb_generic_event_t *event;
    struct port_window *window;

    xcb_flush(port->connection);

    while (port->window_count)
    {
        if (!port->idle_func)
        {
            if (!(event = xcb_wait_for_event(port->connection)))
                break;
        }
        else if (!(event = xcb_poll_for_event(port->connection)))
        {
            port->idle_func(port, port->user_data);
            continue;
        }

        switch (XCB_EVENT_RESPONSE_TYPE(event))
        {
            case XCB_CLIENT_MESSAGE:
                client_message = (xcb_client_message_event_t *)event;
                if (client_message->type == port->wm_protocols_atom
                        && client_message->data.data32[0] == port->wm_delete_window_atom
                        && (window = port_find_window(port, client_message->window)))
                    port_window_destroy(window);
                break;

            case XCB_KEY_PRESS:
                key_press = (xcb_key_press_event_t *)event;
                port->keys[key_press->detail >> 3] |= 1 << (key_press->detail & 0x7);
                break;

            case XCB_KEY_RELEASE:
                key_release = (xcb_key_release_event_t *)event;
                port->keys[key_release->detail >> 3] &= ~(1 << (key_release->detail & 0x7));
                break;

            case XCB_GE_GENERIC:
                generic_event = (xcb_ge_generic_event_t *)event;
                if (generic_event->extension == port->xi_opcode)
                    port_process_xi_event(port, generic_event);
                else
                    fprintf(stderr, "Unhandled generic event %#x/%#x.\n",
                            generic_event->extension, generic_event->event_type);
                break;

            default:
                fprintf(stderr, "Unhandled event type %#x.\n", XCB_EVENT_RESPONSE_TYPE(event));
                break;
        }

        free(event);
    }
}

static bool port_init_xi2(struct port *port)
{
    xcb_input_xi_query_version_cookie_t version_cookie;
    xcb_input_xi_query_version_reply_t *version_reply;
    xcb_query_extension_cookie_t extension_cookie;
    xcb_query_extension_reply_t *extension_reply;
    xcb_screen_t *screen;
    struct
    {
        xcb_input_event_mask_t header;
        uint32_t bits;
    } mask;
    static const char ext_name[] = "XInputExtension";

    if (!(screen = port_get_screen(port->connection, port->screen)))
        return false;

    extension_cookie = xcb_query_extension(port->connection, sizeof(ext_name) - 1, ext_name);
    if (!(extension_reply = xcb_query_extension_reply(port->connection, extension_cookie, NULL))
            || !extension_reply->present)
    {
        free(extension_reply);
        return false;
    }
    port->xi_opcode = extension_reply->major_opcode;
    free(extension_reply);

    version_cookie = xcb_input_xi_query_version(port->connection, 2, 0);
    if (!(version_reply = xcb_input_xi_query_version_reply(port->connection, version_cookie, NULL))
            || version_reply->major_version != 2)
    {
        free(version_reply);
        return false;
    }
    free(version_reply);

    mask.header.deviceid = XCB_INPUT_DEVICE_ALL;
    mask.header.mask_len = 1;
    mask.bits = XCB_INPUT_XI_EVENT_MASK_DEVICE_CHANGED | XCB_INPUT_XI_EVENT_MASK_RAW_MOTION;
    xcb_input_xi_select_events(port->connection, screen->root, 1, &mask.header);

    port->xi_client_pointer = 0;
    port->xi_current_slave = 0;
    memset(&port->mouse, 0, sizeof(port->mouse));

    return true;
}

bool port_init(struct port *port, void *user_data)
{
    if (!(port->connection = xcb_connect(NULL, &port->screen)))
        return false;
    if (xcb_connection_has_error(port->connection) > 0)
        goto fail;
    if ((port->wm_delete_window_atom = port_get_atom(port->connection, "WM_DELETE_WINDOW")) == XCB_NONE)
        goto fail;
    if ((port->wm_protocols_atom = port_get_atom(port->connection, "WM_PROTOCOLS")) == XCB_NONE)
        goto fail;
    if (!port_init_xi2(port))
        goto fail;

    port->windows = NULL;
    port->windows_size = 0;
    port->window_count = 0;
    port->user_data = user_data;
    port->idle_func = NULL;
    memset(port->keys, 0, sizeof(port->keys));

    return true;

fail:
    xcb_disconnect(port->connection);
    return false;
}

void port_grab_pointer(struct port *port, struct port_window *window)
{
    xcb_grab_pointer_cookie_t cookie;
    xcb_cursor_t cursor_id;
    xcb_pixmap_t pixmap_id;
    xcb_screen_t *screen;

    if (!(screen = port_get_screen(port->connection, port->screen)))
        return;

    cursor_id = xcb_generate_id(port->connection);
    pixmap_id = xcb_generate_id(port->connection);

    xcb_create_pixmap(port->connection, 1, pixmap_id, screen->root, 1, 1);
    xcb_create_cursor(port->connection, cursor_id, pixmap_id, pixmap_id, 0, 0, 0, 0, 0, 0, 0, 0);
    xcb_free_pixmap(port->connection, pixmap_id);
    xcb_change_window_attributes(port->connection, window->window, XCB_CW_CURSOR, &cursor_id);
    xcb_free_cursor(port->connection, cursor_id);

    cookie = xcb_grab_pointer(port->connection, 1, window->window, 0,
            XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC, window->window, XCB_NONE, XCB_CURRENT_TIME);
    xcb_discard_reply(port->connection, cookie.sequence);
}

void port_ungrab_pointer(struct port *port, struct port_window *window)
{
    xcb_cursor_t cursor_id = XCB_NONE;

    xcb_change_window_attributes(port->connection, window->window, XCB_CW_CURSOR, &cursor_id);
    xcb_ungrab_pointer(port->connection, XCB_CURRENT_TIME);
}

void port_cleanup(struct port *port)
{
    free(port->windows);
    xcb_disconnect(port->connection);
}

void port_set_idle_func(struct port *port,
        void (*idle_func)(struct port *port, void *user_data))
{
    port->idle_func = idle_func;
}

struct port_swapchain *port_swapchain_create(ID3D12CommandQueue *command_queue,
        struct port_window *window, const struct port_swapchain_desc *desc)
{
    struct vkd3d_image_resource_create_info resource_create_info;
    struct VkSwapchainCreateInfoKHR vk_swapchain_desc;
    struct VkXcbSurfaceCreateInfoKHR surface_desc;
    VkSwapchainKHR vk_swapchain = VK_NULL_HANDLE;
    uint32_t format_count, queue_family_index;
    VkSurfaceCapabilitiesKHR surface_caps;
    VkPhysicalDevice vk_physical_device;
    VkFence vk_fence = VK_NULL_HANDLE;
    struct port_swapchain *swapchain;
    unsigned int image_count, i, j;
    VkFenceCreateInfo fence_desc;
    VkSurfaceFormatKHR *formats;
    ID3D12Device *d3d12_device;
    VkSurfaceKHR vk_surface;
    VkInstance vk_instance;
    VkBool32 supported;
    VkDevice vk_device;
    VkImage *vk_images;
    VkFormat format;

    if ((format = vkd3d_get_vk_format(desc->format)) == VK_FORMAT_UNDEFINED)
        return NULL;

    if (FAILED(command_queue->GetDevice(IID_PPV_ARGS(&d3d12_device))))
        return NULL;

    vk_instance = vkd3d_instance_get_vk_instance(vkd3d_instance_from_device(d3d12_device));
    vk_physical_device = vkd3d_get_vk_physical_device(d3d12_device);
    vk_device = vkd3d_get_vk_device(d3d12_device);

    surface_desc.sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
    surface_desc.pNext = NULL;
    surface_desc.flags = 0;
    surface_desc.connection = window->port->connection;
    surface_desc.window = window->window;
    if (vkCreateXcbSurfaceKHR(vk_instance, &surface_desc, NULL, &vk_surface) < 0)
    {
        d3d12_device->Release();
        return NULL;
    }

    queue_family_index = vkd3d_get_vk_queue_family_index(command_queue);
    if (vkGetPhysicalDeviceSurfaceSupportKHR(vk_physical_device,
            queue_family_index, vk_surface, &supported) < 0 || !supported)
        goto fail;

    if (vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vk_physical_device, vk_surface, &surface_caps) < 0)
        goto fail;

    if ((surface_caps.maxImageCount && desc->buffer_count > surface_caps.maxImageCount)
            || desc->buffer_count < surface_caps.minImageCount
            || desc->width > surface_caps.maxImageExtent.width || desc->width < surface_caps.minImageExtent.width
            || desc->height > surface_caps.maxImageExtent.height || desc->height < surface_caps.minImageExtent.height
            || !(surface_caps.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR))
        goto fail;

    if (vkGetPhysicalDeviceSurfaceFormatsKHR(vk_physical_device, vk_surface, &format_count, NULL) < 0
            || !format_count || !(formats = (VkSurfaceFormatKHR *)calloc(format_count, sizeof(*formats))))
        goto fail;

    if (vkGetPhysicalDeviceSurfaceFormatsKHR(vk_physical_device, vk_surface, &format_count, formats) < 0)
    {
        free(formats);
        goto fail;
    }

    if (format_count != 1 || formats->format != VK_FORMAT_UNDEFINED
            || formats->colorSpace != VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
    {
        for (i = 0; i < format_count; ++i)
        {
            if (formats[i].format == format && formats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
                break;
        }

        if (i == format_count)
        {
            free(formats);
            goto fail;
        }
    }

    free(formats);
    formats = NULL;

    vk_swapchain_desc.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    vk_swapchain_desc.pNext = NULL;
    vk_swapchain_desc.flags = 0;
    vk_swapchain_desc.surface = vk_surface;
    vk_swapchain_desc.minImageCount = desc->buffer_count;
    vk_swapchain_desc.imageFormat = format;
    vk_swapchain_desc.imageColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
    vk_swapchain_desc.imageExtent.width = desc->width;
    vk_swapchain_desc.imageExtent.height = desc->height;
    vk_swapchain_desc.imageArrayLayers = 1;
    vk_swapchain_desc.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    vk_swapchain_desc.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    vk_swapchain_desc.queueFamilyIndexCount = 0;
    vk_swapchain_desc.pQueueFamilyIndices = NULL;
    vk_swapchain_desc.preTransform = surface_caps.currentTransform;
    vk_swapchain_desc.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    vk_swapchain_desc.presentMode = VK_PRESENT_MODE_FIFO_KHR;
    vk_swapchain_desc.clipped = VK_TRUE;
    vk_swapchain_desc.oldSwapchain = VK_NULL_HANDLE;
    if (vkCreateSwapchainKHR(vk_device, &vk_swapchain_desc, NULL, &vk_swapchain) < 0)
        goto fail;

    fence_desc.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fence_desc.pNext = NULL;
    fence_desc.flags = 0;
    if (vkCreateFence(vk_device, &fence_desc, NULL, &vk_fence) < 0)
        goto fail;

    if (vkGetSwapchainImagesKHR(vk_device, vk_swapchain, &image_count, NULL) < 0
            || !(vk_images = (VkImage *)calloc(image_count, sizeof(*vk_images))))
        goto fail;

    if (vkGetSwapchainImagesKHR(vk_device, vk_swapchain, &image_count, vk_images) < 0)
    {
        free(vk_images);
        goto fail;
    }

    if (!(swapchain = (struct port_swapchain *)malloc(offsetof(struct port_swapchain,
            buffers) + image_count * sizeof(*swapchain->buffers))))
    {
        free(vk_images);
        goto fail;
    }
    swapchain->vk_surface = vk_surface;
    swapchain->vk_swapchain = vk_swapchain;
    swapchain->vk_fence = vk_fence;
    swapchain->vk_instance = vk_instance;
    swapchain->vk_device = vk_device;

    vkAcquireNextImageKHR(vk_device, vk_swapchain, UINT64_MAX,
            VK_NULL_HANDLE, vk_fence, &swapchain->current_buffer);
    vkWaitForFences(vk_device, 1, &vk_fence, VK_TRUE, UINT64_MAX);
    vkResetFences(vk_device, 1, &vk_fence);

    resource_create_info.type = VKD3D_STRUCTURE_TYPE_IMAGE_RESOURCE_CREATE_INFO;
    resource_create_info.next = NULL;
    resource_create_info.desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
    resource_create_info.desc.Alignment = 0;
    resource_create_info.desc.Width = desc->width;
    resource_create_info.desc.Height = desc->height;
    resource_create_info.desc.DepthOrArraySize = 1;
    resource_create_info.desc.MipLevels = 1;
    resource_create_info.desc.Format = desc->format;
    resource_create_info.desc.SampleDesc.Count = 1;
    resource_create_info.desc.SampleDesc.Quality = 0;
    resource_create_info.desc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
    resource_create_info.desc.Flags = D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;
    resource_create_info.flags = VKD3D_RESOURCE_INITIAL_STATE_TRANSITION | VKD3D_RESOURCE_PRESENT_STATE_TRANSITION;
    resource_create_info.present_state = D3D12_RESOURCE_STATE_PRESENT;
    for (i = 0; i < image_count; ++i)
    {
        resource_create_info.vk_image = vk_images[i];
        if (FAILED(vkd3d_create_image_resource(d3d12_device, &resource_create_info, &swapchain->buffers[i])))
        {
            for (j = 0; j < i; ++j)
            {
                swapchain->buffers[j]->Release();
            }
            free(swapchain);
            free(vk_images);
            goto fail;
        }
    }
    swapchain->buffer_count = image_count;
    free(vk_images);
    d3d12_device->Release();

    swapchain->command_queue = command_queue;
    swapchain->command_queue->AddRef();

    return swapchain;

fail:
    if (vk_fence != VK_NULL_HANDLE)
        vkDestroyFence(vk_device, vk_fence, NULL);
    if (vk_swapchain != VK_NULL_HANDLE)
        vkDestroySwapchainKHR(vk_device, vk_swapchain, NULL);
    vkDestroySurfaceKHR(vk_instance, vk_surface, NULL);
    d3d12_device->Release();
    return NULL;
}

ID3D12Resource *port_swapchain_get_back_buffer(struct port_swapchain *swapchain, unsigned int index)
{
    ID3D12Resource *resource = NULL;

    if (index < swapchain->buffer_count && (resource = swapchain->buffers[index]))
        resource->AddRef();

    return resource;
}

void port_swapchain_present(struct port_swapchain *swapchain)
{
    VkPresentInfoKHR present_desc;
    VkQueue vk_queue;

    present_desc.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    present_desc.pNext = NULL;
    present_desc.waitSemaphoreCount = 0;
    present_desc.pWaitSemaphores = NULL;
    present_desc.swapchainCount = 1;
    present_desc.pSwapchains = &swapchain->vk_swapchain;
    present_desc.pImageIndices = &swapchain->current_buffer;
    present_desc.pResults = NULL;

    vk_queue = vkd3d_acquire_vk_queue(swapchain->command_queue);
    vkQueuePresentKHR(vk_queue, &present_desc);
    vkd3d_release_vk_queue(swapchain->command_queue);

    vkAcquireNextImageKHR(swapchain->vk_device, swapchain->vk_swapchain, UINT64_MAX,
            VK_NULL_HANDLE, swapchain->vk_fence, &swapchain->current_buffer);
    vkWaitForFences(swapchain->vk_device, 1, &swapchain->vk_fence, VK_TRUE, UINT64_MAX);
    vkResetFences(swapchain->vk_device, 1, &swapchain->vk_fence);
}

void port_swapchain_destroy(struct port_swapchain *swapchain)
{
    unsigned int i;

    swapchain->command_queue->Release();
    for (i = 0; i < swapchain->buffer_count; ++i)
    {
        swapchain->buffers[i]->Release();
    }
    vkDestroyFence(swapchain->vk_device, swapchain->vk_fence, NULL);
    vkDestroySwapchainKHR(swapchain->vk_device, swapchain->vk_swapchain, NULL);
    vkDestroySurfaceKHR(swapchain->vk_instance, swapchain->vk_surface, NULL);
    free(swapchain);
}

struct port_key_map *port_get_key_map(struct port *port)
{
    xcb_get_keyboard_mapping_cookie_t cookie;
    xcb_get_keyboard_mapping_reply_t *reply;
    const xcb_keysym_t *keysyms;
    struct port_key_map *map;
    const xcb_setup_t *setup;
    size_t i, count;

    setup = xcb_get_setup(port->connection);
    count = setup->max_keycode - setup->min_keycode + 1;
    if (!(map = (typeof(map))malloc(offsetof(typeof(*map), symbols) + count * sizeof(*map->symbols))))
        return NULL;

    cookie = xcb_get_keyboard_mapping(port->connection, setup->min_keycode, count);
    if (!(reply = xcb_get_keyboard_mapping_reply(port->connection, cookie, NULL)))
        return NULL;

    map->base = setup->min_keycode;
    map->count = count;

    keysyms = xcb_get_keyboard_mapping_keysyms(reply);
    for (i = 0; i < map->count; ++i)
    {
        map->symbols[i] = keysyms[i * reply->keysyms_per_keycode];
    }
    free(reply);

    return map;
}

bool port_get_key_state(const struct port *port, unsigned int key)
{
    return port->keys[key >> 3] & (1 << (key & 0x7));
}

void port_get_mouse_state(struct port *port, struct port_mouse_state *s)
{
    *s = port->mouse;
    memset(&port->mouse, 0, sizeof(port->mouse));
}

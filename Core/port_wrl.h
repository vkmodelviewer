/*
 * Copyright 2017 Henri Verbeet for CodeWeavers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/* This file is derived from the ComPtr implementation in the mingw-w64
 * runtime package, which has no copyright assigned and is placed in the
 * Public Domain. */

#ifndef PORT_WRL_H
#define PORT_WRL_H

#include <stddef.h>

namespace Microsoft
{
    namespace WRL
    {
        namespace Details
        {
            struct BoolStruct
            {
                int Member;
            };

            typedef int BoolStruct::*BoolType;

            template <typename T> class ComPtrRefBase
            {
            protected:
                T *ptr_;

            public:
                typedef typename T::InterfaceType InterfaceType;

                operator IUnknown **() const throw()
                {
                    static_assert(__is_base_of(IUnknown, InterfaceType), "Invalid cast");
                    return reinterpret_cast<IUnknown **>(ptr_->ReleaseAndGetAddressOf());
                }
            };

            template <typename T> class ComPtrRef : public Details::ComPtrRefBase<T>
            {
            public:
                ComPtrRef(T *ptr) throw()
                {
                    ComPtrRefBase<T>::ptr_ = ptr;
                }

                operator void **() const throw()
                {
                    return reinterpret_cast<void **>(ComPtrRefBase<T>::ptr_->ReleaseAndGetAddressOf());
                }

                operator T *() throw()
                {
                    *ComPtrRefBase<T>::ptr_ = nullptr;
                    return ComPtrRefBase<T>::ptr_;
                }

                operator typename ComPtrRefBase<T>::InterfaceType **() throw()
                {
                    return ComPtrRefBase<T>::ptr_->ReleaseAndGetAddressOf();
                }

                typename ComPtrRefBase<T>::InterfaceType *operator *() throw()
                {
                    return ComPtrRefBase<T>::ptr_->Get();
                }

                typename ComPtrRefBase<T>::InterfaceType *const *GetAddressOf() const throw()
                {
                    return ComPtrRefBase<T>::ptr_->GetAddressOf();
                }

                typename ComPtrRefBase<T>::InterfaceType **ReleaseAndGetAddressOf() throw()
                {
                    return ComPtrRefBase<T>::ptr_->ReleaseAndGetAddressOf();
                }
            };
        }

        template<typename T> class ComPtr
        {
        public:
            typedef T InterfaceType;

            ComPtr() throw() : ptr_(nullptr) {}
            ComPtr(decltype(nullptr)) throw() : ptr_(nullptr) {}

            template<class U> ComPtr(U *other) throw() : ptr_(other)
            {
                InternalAddRef();
            }

            ComPtr(const ComPtr &other) throw() : ptr_(other.ptr_)
            {
                InternalAddRef();
            }

            template<class U> ComPtr(const ComPtr<U> &other) throw() : ptr_(other.Get())
            {
                InternalAddRef();
            }

            ComPtr(ComPtr &&other) throw() : ptr_(nullptr)
            {
                if (this != reinterpret_cast<ComPtr *>(&reinterpret_cast<unsigned char &>(other)))
                    Swap(other);
            }

            template<class U> ComPtr(ComPtr<U>&& other) throw() : ptr_(other.Detach()) {}

            ~ComPtr() throw()
            {
                InternalRelease();
            }

            ComPtr &operator =(decltype(nullptr)) throw()
            {
                InternalRelease();
                return *this;
            }

            ComPtr &operator =(InterfaceType *other) throw()
            {
                if (ptr_ != other)
                {
                    InternalRelease();
                    ptr_ = other;
                    InternalAddRef();
                }
                return *this;
            }

            template<typename U> ComPtr &operator =(U *other) throw()
            {
                if (ptr_ != other)
                {
                    InternalRelease();
                    ptr_ = other;
                    InternalAddRef();
                }
                return *this;
            }

            ComPtr &operator =(const ComPtr &other) throw()
            {
                if (ptr_ != other.ptr_)
                    ComPtr(other).Swap(*this);
                return *this;
            }

            template<class U> ComPtr &operator =(const ComPtr<U> &other) throw()
            {
                ComPtr(other).Swap(*this);
                return *this;
            }

            ComPtr &operator =(ComPtr &&other) throw()
            {
                ComPtr(other).Swap(*this);
                return *this;
            }

            template<class U> ComPtr &operator =(ComPtr<U> &&other) throw()
            {
                ComPtr(other).Swap(*this);
                return *this;
            }

            void Swap(ComPtr &&r) throw()
            {
                InterfaceType *tmp = ptr_;
                ptr_ = r.ptr_;
                r.ptr_ = tmp;
            }

            void Swap(ComPtr &r) throw()
            {
                InterfaceType *tmp = ptr_;
                ptr_ = r.ptr_;
                r.ptr_ = tmp;
            }

            operator Details::BoolType() const throw()
            {
                return Get() != nullptr ? &Details::BoolStruct::Member : nullptr;
            }

            InterfaceType *Get() const throw()
            {
                return ptr_;
            }

            InterfaceType *operator ->() const throw()
            {
                return ptr_;
            }

            Details::ComPtrRef<ComPtr<T>> operator &() throw()
            {
                return Details::ComPtrRef<ComPtr<T>>(this);
            }

            const Details::ComPtrRef<const ComPtr<T>> operator &() const throw()
            {
                return Details::ComPtrRef<const ComPtr<T>>(this);
            }

            InterfaceType *const *GetAddressOf() const throw()
            {
                return &ptr_;
            }

            InterfaceType **GetAddressOf() throw()
            {
                return &ptr_;
            }

            InterfaceType **ReleaseAndGetAddressOf() throw()
            {
                InternalRelease();
                return &ptr_;
            }

            InterfaceType *Detach() throw()
            {
                T *ptr = ptr_;
                ptr_ = nullptr;
                return ptr;
            }

            void Attach(InterfaceType *other) throw()
            {
                if (ptr_ != other)
                {
                    InternalRelease();
                    ptr_ = other;
                    InternalAddRef();
                }
            }

        protected:
            InterfaceType *ptr_;

            void InternalAddRef() const throw()
            {
                if (ptr_)
                    ptr_->AddRef();
            }

            unsigned long InternalRelease() throw()
            {
                InterfaceType *tmp = ptr_;
                if (!tmp)
                    return 0;
                ptr_ = nullptr;
                return tmp->Release();
            }
        };
    }
}

#endif /* PORT_WRL_H */

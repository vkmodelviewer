//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
// Developed by Minigraph
//
// Author:  James Stanard
//

#include "pch.h"
#include "GameCore.h"
#include "GameInput.h"

namespace GameCore
{
    extern struct port port;
    extern struct port_window *window;
}

namespace
{
    bool s_Buttons[2][GameInput::kNumDigitalInputs];
    float s_HoldDuration[GameInput::kNumDigitalInputs] = { 0.0f };
    float s_Analogs[GameInput::kNumAnalogInputs];
    float s_AnalogsTC[GameInput::kNumAnalogInputs];

    unsigned char s_DXKeyMapping[GameInput::kNumKeys]; // map DigitalInput enum to DX key codes
}

void GameInput::Initialize()
{
    struct port_key_map *map;
    unsigned int i, j;

    static const uint16_t syms[] =
    {
        [GameInput::kKey_escape]        = PORT_KEY_Escape,
        [GameInput::kKey_1]             = PORT_KEY_1,
        [GameInput::kKey_2]             = PORT_KEY_2,
        [GameInput::kKey_3]             = PORT_KEY_3,
        [GameInput::kKey_4]             = PORT_KEY_4,
        [GameInput::kKey_5]             = PORT_KEY_5,
        [GameInput::kKey_6]             = PORT_KEY_6,
        [GameInput::kKey_7]             = PORT_KEY_7,
        [GameInput::kKey_8]             = PORT_KEY_8,
        [GameInput::kKey_9]             = PORT_KEY_9,
        [GameInput::kKey_0]             = PORT_KEY_0,
        [GameInput::kKey_minus]         = PORT_KEY_minus,
        [GameInput::kKey_equals]        = PORT_KEY_equal,
        [GameInput::kKey_back]          = PORT_KEY_BackSpace,
        [GameInput::kKey_tab]           = PORT_KEY_Tab,
        [GameInput::kKey_q]             = PORT_KEY_q,
        [GameInput::kKey_w]             = PORT_KEY_w,
        [GameInput::kKey_e]             = PORT_KEY_e,
        [GameInput::kKey_r]             = PORT_KEY_r,
        [GameInput::kKey_t]             = PORT_KEY_t,
        [GameInput::kKey_y]             = PORT_KEY_y,
        [GameInput::kKey_u]             = PORT_KEY_u,
        [GameInput::kKey_i]             = PORT_KEY_i,
        [GameInput::kKey_o]             = PORT_KEY_o,
        [GameInput::kKey_p]             = PORT_KEY_p,
        [GameInput::kKey_lbracket]      = PORT_KEY_bracketleft,
        [GameInput::kKey_rbracket]      = PORT_KEY_bracketright,
        [GameInput::kKey_return]        = PORT_KEY_Return,
        [GameInput::kKey_lcontrol]      = PORT_KEY_Control_L,
        [GameInput::kKey_a]             = PORT_KEY_a,
        [GameInput::kKey_s]             = PORT_KEY_s,
        [GameInput::kKey_d]             = PORT_KEY_d,
        [GameInput::kKey_f]             = PORT_KEY_f,
        [GameInput::kKey_g]             = PORT_KEY_g,
        [GameInput::kKey_h]             = PORT_KEY_h,
        [GameInput::kKey_j]             = PORT_KEY_j,
        [GameInput::kKey_k]             = PORT_KEY_k,
        [GameInput::kKey_l]             = PORT_KEY_l,
        [GameInput::kKey_semicolon]     = PORT_KEY_semicolon,
        [GameInput::kKey_apostrophe]    = PORT_KEY_apostrophe,
        [GameInput::kKey_grave]         = PORT_KEY_grave,
        [GameInput::kKey_lshift]        = PORT_KEY_Shift_L,
        [GameInput::kKey_backslash]     = PORT_KEY_backslash,
        [GameInput::kKey_z]             = PORT_KEY_z,
        [GameInput::kKey_x]             = PORT_KEY_x,
        [GameInput::kKey_c]             = PORT_KEY_c,
        [GameInput::kKey_v]             = PORT_KEY_v,
        [GameInput::kKey_b]             = PORT_KEY_b,
        [GameInput::kKey_n]             = PORT_KEY_n,
        [GameInput::kKey_m]             = PORT_KEY_m,
        [GameInput::kKey_comma]         = PORT_KEY_comma,
        [GameInput::kKey_period]        = PORT_KEY_period,
        [GameInput::kKey_slash]         = PORT_KEY_slash,
        [GameInput::kKey_rshift]        = PORT_KEY_Shift_R,
        [GameInput::kKey_multiply]      = PORT_KEY_KP_Multiply,
        [GameInput::kKey_lalt]          = PORT_KEY_Alt_L,
        [GameInput::kKey_space]         = PORT_KEY_space,
        [GameInput::kKey_capital]       = PORT_KEY_Caps_Lock,
        [GameInput::kKey_f1]            = PORT_KEY_F1,
        [GameInput::kKey_f2]            = PORT_KEY_F2,
        [GameInput::kKey_f3]            = PORT_KEY_F3,
        [GameInput::kKey_f4]            = PORT_KEY_F4,
        [GameInput::kKey_f5]            = PORT_KEY_F5,
        [GameInput::kKey_f6]            = PORT_KEY_F6,
        [GameInput::kKey_f7]            = PORT_KEY_F7,
        [GameInput::kKey_f8]            = PORT_KEY_F8,
        [GameInput::kKey_f9]            = PORT_KEY_F9,
        [GameInput::kKey_f10]           = PORT_KEY_F10,
        [GameInput::kKey_numlock]       = PORT_KEY_Num_Lock,
        [GameInput::kKey_scroll]        = PORT_KEY_Scroll_Lock,
        [GameInput::kKey_numpad7]       = PORT_KEY_KP_Home,
        [GameInput::kKey_numpad8]       = PORT_KEY_KP_Up,
        [GameInput::kKey_numpad9]       = PORT_KEY_KP_Page_Up,
        [GameInput::kKey_subtract]      = PORT_KEY_KP_Subtract,
        [GameInput::kKey_numpad4]       = PORT_KEY_KP_Left,
        [GameInput::kKey_numpad5]       = PORT_KEY_KP_Begin,
        [GameInput::kKey_numpad6]       = PORT_KEY_KP_Right,
        [GameInput::kKey_add]           = PORT_KEY_KP_Add,
        [GameInput::kKey_numpad1]       = PORT_KEY_KP_End,
        [GameInput::kKey_numpad2]       = PORT_KEY_KP_Down,
        [GameInput::kKey_numpad3]       = PORT_KEY_KP_Page_Down,
        [GameInput::kKey_numpad0]       = PORT_KEY_KP_Insert,
        [GameInput::kKey_decimal]       = PORT_KEY_KP_Delete,
        [GameInput::kKey_f11]           = PORT_KEY_F11,
        [GameInput::kKey_f12]           = PORT_KEY_F12,
        [GameInput::kKey_numpadenter]   = PORT_KEY_KP_Enter,
        [GameInput::kKey_rcontrol]      = PORT_KEY_Control_R,
        [GameInput::kKey_divide]        = PORT_KEY_KP_Divide,
        [GameInput::kKey_sysrq]         = PORT_KEY_Print,
        [GameInput::kKey_ralt]          = PORT_KEY_Alt_R,
        [GameInput::kKey_pause]         = PORT_KEY_Pause,
        [GameInput::kKey_home]          = PORT_KEY_Home,
        [GameInput::kKey_up]            = PORT_KEY_Up,
        [GameInput::kKey_pgup]          = PORT_KEY_Page_Up,
        [GameInput::kKey_left]          = PORT_KEY_Left,
        [GameInput::kKey_right]         = PORT_KEY_Right,
        [GameInput::kKey_end]           = PORT_KEY_End,
        [GameInput::kKey_down]          = PORT_KEY_Down,
        [GameInput::kKey_pgdn]          = PORT_KEY_Page_Down,
        [GameInput::kKey_insert]        = PORT_KEY_Insert,
        [GameInput::kKey_delete]        = PORT_KEY_Delete,
        [GameInput::kKey_lwin]          = PORT_KEY_Super_L,
        [GameInput::kKey_rwin]          = PORT_KEY_Super_R,
        [GameInput::kKey_apps]          = PORT_KEY_Menu,
    };

    ASSERT(map = port_get_key_map(&GameCore::port));

    memset(s_DXKeyMapping, 0, sizeof(s_DXKeyMapping));
    for (i = 0; i < map->count; ++i)
    {
        for (j = 0; j < _countof(syms); ++j)
        {
            if (syms[j] != map->symbols[i])
                continue;

            s_DXKeyMapping[j] = map->base + i;
            break;
        }
    }

    free(map);

    memset(s_Buttons, 0, sizeof(s_Buttons));
    memset(s_Analogs, 0, sizeof(s_Analogs));

    port_grab_pointer(&GameCore::port, GameCore::window);
}

void GameInput::Shutdown()
{
    port_ungrab_pointer(&GameCore::port, GameCore::window);
}

void GameInput::Update( float frameDelta )
{
    struct port_mouse_state mouse_state;
    unsigned int i;

    memcpy(s_Buttons[1], s_Buttons[0], sizeof(s_Buttons[0]));
    memset(s_Buttons[0], 0, sizeof(s_Buttons[0]));
    memset(s_Analogs, 0, sizeof(s_Analogs));

    for (i = 0; i < kNumKeys; ++i)
    {
        s_Buttons[0][i] = port_get_key_state(&GameCore::port, s_DXKeyMapping[i]);
    }

    port_get_mouse_state(&GameCore::port, &mouse_state);
    for (uint32_t i = 0; i < 8; ++i)
    {
        if (mouse_state.buttons & (1u << i))
            s_Buttons[0][kMouse0 + i] = true;
    }

    s_Analogs[kAnalogMouseX] = mouse_state.x * .0018f;
    s_Analogs[kAnalogMouseY] = mouse_state.y * -.0018f;

    if (mouse_state.z > 0)
        s_Analogs[kAnalogMouseScroll] = 1.0f;
    else if (mouse_state.z < 0)
        s_Analogs[kAnalogMouseScroll] = -1.0f;

    // Update time duration for buttons pressed
    for (uint32_t i = 0; i < kNumDigitalInputs; ++i)
    {
        if (s_Buttons[0][i])
        {
            if (!s_Buttons[1][i])
                s_HoldDuration[i] = 0.0f;
            else
                s_HoldDuration[i] += frameDelta;
        }
    }

    for (uint32_t i = 0; i < kNumAnalogInputs; ++i)
    {
        s_AnalogsTC[i] = s_Analogs[i] * frameDelta;
    }
}

bool GameInput::IsAnyPressed( void )
{
    return s_Buttons[0] != 0;
}

bool GameInput::IsPressed( DigitalInput di )
{
    return s_Buttons[0][di];
}

bool GameInput::IsFirstPressed( DigitalInput di )
{
    return s_Buttons[0][di] && !s_Buttons[1][di];
}

bool GameInput::IsReleased( DigitalInput di )
{
    return !s_Buttons[0][di];
}

bool GameInput::IsFirstReleased( DigitalInput di )
{
    return !s_Buttons[0][di] && s_Buttons[1][di];
}

float GameInput::GetDurationPressed( DigitalInput di )
{
    return s_HoldDuration[di];
}

float GameInput::GetAnalogInput( AnalogInput ai )
{
    return s_Analogs[ai];
}

float GameInput::GetTimeCorrectedAnalogInput( AnalogInput ai )
{
    return s_AnalogsTC[ai];
}

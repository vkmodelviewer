dnl
dnl This file contains code derived from WINE_TRY_CFLAGS() and is used courtesy of Alexandre Julliard.
dnl

dnl MODELVIEWER_CHECK_CXXFLAGS(flags)
AC_DEFUN([MODELVIEWER_CHECK_CXXFLAGS],
  [AS_VAR_PUSHDEF([modelviewer_cxxflags], modelviewer_cv_cxxflags_[[$1]])dnl
  AC_CACHE_CHECK([whether the compiler supports $1], modelviewer_cxxflags,
  [modelviewer_cxxflags_saved=$CXXFLAGS
  CXXFLAGS="$CXXFLAGS $1 -Werror"
  AC_LINK_IFELSE([AC_LANG_SOURCE([[int main(int argc, char **argv) { return 0; }]])],
                 [AS_VAR_SET(modelviewer_cxxflags, yes)],
                 [AS_VAR_SET(modelviewer_cxxflags, no)])
  CXXFLAGS=$modelviewer_cxxflags_saved])
  AS_VAR_IF([modelviewer_cxxflags], [yes], [MODELVIEWER_CXXFLAGS="$MODELVIEWER_CXXFLAGS $1"])dnl
  AS_VAR_POPDEF([modelviewer_cxxflags])])
